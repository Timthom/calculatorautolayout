//
//  ViewController.swift
//  Auto Layout Calculator
//
//  Created by Thomas on 2018-08-20.
//  Copyright © 2018 Thomas Månsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var displayLabel: UILabel!

    @IBOutlet private weak var dotButton: UIButton!
    
    private var status: Bool = true
    
    private var isFinishTypingNumbers: Bool = true
    
    private var displayValue: Double {
        get {
            let number = Double(displayLabel.text!)
       
//            guard let number = Double(displayLabel.text!)  else {
//                fatalError("Cannot convert display label text to a double.")
//            }
            
            
            return number ?? 0.0
        }
        set {
            displayLabel.text = String(newValue)
        }
    }
    
    private var calculator = CalculatorLogic()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dotButton.isEnabled = false
    
    }
    
    
    
    @IBAction private func calcButtonPressed(_ sender: UIButton) {
        
        
        isFinishTypingNumbers = true
        
        calculator.setNumber(displayValue)
    
        
        if let calcMethod = sender.currentTitle {
            
            if let result = calculator.calculate(symbol: calcMethod)  {
                
                displayValue = result
 
            }

        }
        dotButton.isEnabled = false
    }
    
    
    @IBAction private func numButtonPressed(_ sender: UIButton) {
        
        if let numValue = sender.currentTitle  {
           
            if isFinishTypingNumbers {
                displayLabel.text = numValue
                isFinishTypingNumbers = false
                } else {
//
//                if numValue == "." {
//
//                     let isInt = floor(displayValue) == displayValue
//
//                    if !isInt {
//                        return
//                    }
//                }
               displayLabel.text = displayLabel.text! + numValue
           }
            dotButton.isEnabled = true
        }
        
    }
    
    
    @IBAction private func dotButtonPressed(_ sender: UIButton) {
        if let numValue = sender.currentTitle  {
            
                if numValue == "." {
                    
                    let isInt = floor(displayValue) == displayValue
                    
                    if !isInt {
                        return
                    }
                    dotButton.isEnabled = false
                }
                displayLabel.text = displayLabel.text! + numValue
            
        }
    }
    
}

